package com.team.scrub.angleseadrugcalculator;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import android.content.Intent;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import com.kyanogen.signatureview.SignatureView;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;


public class NurseNewUser extends AppCompatActivity {

    //Declaring Variables

    EditText etName, etRNNumber, etPassword, etconfirmPassword;
    Button btnNewUser;
    Button btnclrSig;
    SignatureView signatureView;
    Bitmap bitmap;
    Realm realm;
    String Name, RNNumber, Password, confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        realm.init(this);
        setContentView(R.layout.new_nurse_login);
        setTitle("Account Creation");
        //initials variables for use
        etName = (EditText) findViewById(R.id.et_nurse_name);
        etRNNumber = (EditText) findViewById(R.id.et_rn_number_new);
        etPassword = (EditText) findViewById(R.id.et_password_new);
        etconfirmPassword = (EditText) findViewById(R.id.et_confirm_password);
        signatureView = (SignatureView) findViewById(R.id.signature_view);
        bitmap = signatureView.getSignatureBitmap();
        btnNewUser = (Button) findViewById(R.id.btn_nurse_logon);
        btnclrSig = (Button) findViewById(R.id.btn_clear_sig);

        realm = Realm.getDefaultInstance();
        btnclrSig.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                signatureView.clearCanvas();
            }
        });

        btnNewUser.setOnClickListener(new View.OnClickListener() {
            //Get strings and check if they are empty
            @Override
            public void onClick(View v) {
                Name = etName.getText().toString();
                RNNumber = etRNNumber.getText().toString();
                Password = etPassword.getText().toString();
                confirmPassword = etconfirmPassword.getText().toString();

                if (TextUtils.isEmpty(Name)) {
                    Toast.makeText(NurseNewUser.this, "Oops, you forgot to fill in some fields!", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(RNNumber)) {
                    Toast.makeText(NurseNewUser.this, "Oops, you forgot to fill in some fields!", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(Password)) {
                    Toast.makeText(NurseNewUser.this, "Oops, you forgot to fill in some fields!", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(confirmPassword)) {
                    Toast.makeText(NurseNewUser.this, "Oops, you forgot to fill in some fields!", Toast.LENGTH_SHORT).show();
                } else if (signatureView.isBitmapEmpty()) {
                    Toast.makeText(NurseNewUser.this, "Please insert a signature", Toast.LENGTH_SHORT).show();
                } else {
                    if (!Password.equals(confirmPassword)) {
                        Toast.makeText(NurseNewUser.this, "Please make sure you enter the same passwords!", Toast.LENGTH_SHORT).show();

                    } else {

                        submit();
                        final RealmResults<Nurse> count = realm.where(Nurse.class).equalTo("isActive", TRUE).findAll();
                        Toast.makeText(NurseNewUser.this, "Nurse " + Name + " has been Added login now please", Toast.LENGTH_SHORT).show();
                        Intent first = new Intent(NurseNewUser.this, NurseLogon.class);
                        startActivity(first);
                        }
                    }
                }
            });
        }

    public static byte[] getBytesFromBitmap(Bitmap bm){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 64, stream);
        return stream.toByteArray();
    }
    public void submit(){

        Name = etName.getText().toString();
        RNNumber = etRNNumber.getText().toString();
        Password = etPassword.getText().toString();
        confirmPassword = etconfirmPassword.getText().toString();

        Bitmap bitmap = signatureView.getSignatureBitmap();
        byte[] bytes = getBytesFromBitmap(bitmap);
        realm.beginTransaction();
        Nurse n = new Nurse();

        n.setName(Name);
        n.setPassword(Password);
        n.setRnNumber(RNNumber);
        n.setNurseSig(bytes);
        n.setActive(FALSE);
        realm.copyToRealm(n);
        realm.commitTransaction();

    }
}
