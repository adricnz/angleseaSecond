package com.team.scrub.angleseadrugcalculator;

import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class NurseLogon extends AppCompatActivity {
    public Realm realm;
    public RealmConfiguration config;

    //Declare Variables
    Button btnlogon, btnAdduser;
    EditText etrnNumber, etPassword;
    String rnNumber, password, Name;
    RealmQuery<Nurse> nurse;


    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.nurse_logon);
        setTitle("Nurse Login");
        btnAdduser = (Button) findViewById(R.id.btn_goto_add_new);
        btnlogon = (Button) findViewById(R.id.btn_nurse_logon);
        realm.init(this);
        etrnNumber = (EditText) findViewById(R.id.edit_nl_rn);
        etPassword = (EditText) findViewById(R.id.et_password);
        realm = Realm.getDefaultInstance();

        final RealmResults<Nurse> count = realm.where(Nurse.class).equalTo("isActive", Boolean.TRUE).findAll();
        if (count.size() == 2) {
            Toast.makeText(NurseLogon.this, "Please log out before attempting an additional logon", Toast.LENGTH_SHORT).show();
            Intent test = new Intent(NurseLogon.this, MainActivity.class);
            startActivity(test);
        }
        //Logon Button, checks user is valid
        btnlogon.setOnClickListener(new View.OnClickListener() {
            //Get inputs and check if they are empty
            @Override
            public void onClick(View v) {

                rnNumber = etrnNumber.getText().toString();
                Nurse n;
                final RealmResults<Nurse> nurses = realm.where(Nurse.class).equalTo("rnNumber", rnNumber).findAll();
                if(!nurses.isEmpty()){

                    n = nurses.get(0);
                    password = n.getPassword();
                    Name = n.getName();

    //Checks Password is correct
                    if (n.getPassword().equals(etPassword.getText().toString())) {
                        Toast.makeText(NurseLogon.this, "Nurse " + Name + " Has signed in", Toast.LENGTH_SHORT).show();
                        realm.beginTransaction();
                        n.setActive(true);
                        realm.commitTransaction();
                        Intent intent = new Intent(NurseLogon.this, MainActivity.class);
                        startActivity(intent);
                    } else if (!n.getPassword().equals(etPassword.getText().toString())) {
                        Toast.makeText(NurseLogon.this, "Incorrect password. Please Try Again", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(NurseLogon.this, "User Not found. Please Try Again", Toast.LENGTH_SHORT).show();


                }

            }
        });
        btnAdduser.setOnClickListener(new View.OnClickListener() {
            //Get inputs and check if they are empty
            @Override
            public void onClick(View v) {
                Intent AddNew = new Intent(NurseLogon.this, NurseNewUser.class);
                startActivity(AddNew);

            }
        });
    }
}

