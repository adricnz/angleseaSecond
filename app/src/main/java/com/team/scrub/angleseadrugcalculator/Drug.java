package com.team.scrub.angleseadrugcalculator;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;

/**
 * Created by student on 6/14/2018.
 */

public class Drug extends RealmObject {
    @PrimaryKey
    private int id;
    private String name;
    private float ml;
    private float mg;
    private String ageGroup;

    public Drug() {
        //id = getNextKey();
    }
    public int getNextKey() {
        Realm realm = Realm.getDefaultInstance();
        try {
            Number n = realm.where(Drug.class).max("id");
            if (n != null) {
                return n.intValue() + 1;
            } else {
                return 0;
            }
        }
        catch(ArrayIndexOutOfBoundsException e){
            return 0;
        }

    }

    public String getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(String ageGroup) {
        this.ageGroup = ageGroup;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getMl() {
        return ml;
    }

    public void setMl(float ml) {
        this.ml = ml;
    }

    public float getMg() {
        return mg;
    }

    public void setMg(float mg) {
        this.mg = mg;
    }
}
