package com.team.scrub.angleseadrugcalculator;


import android.content.Context;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomePage extends android.app.Fragment {
    View rootView;

    private RecyclerView mRecyclerView;
    private HomePageListAdapter mAdapter;
    private List<Prescription> prescriptionList = new ArrayList<>();
    Realm realm;
    ImageButton btnAddDrug, btnAddPatient, btnAdminister;

    preSwitchListener mCallBack;

    public interface preSwitchListener {
        public void swapToPreVerification(int pId);
        public void switchToIntent(String intent);

    }

    public HomePage() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_home_page, container, false);
        realm = Realm.getDefaultInstance();

        //recycler code
        mAdapter = new HomePageListAdapter(prescriptionList);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.pl_prescription_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);

        btnAddDrug = (ImageButton) rootView.findViewById(R.id.btn_home_add_drug);
        btnAddPatient = (ImageButton) rootView.findViewById(R.id.btn_home_add_patient);
        btnAdminister = (ImageButton) rootView.findViewById(R.id.btn_home_administer);

        btnAddDrug.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mCallBack.switchToIntent("drug");
            }
        });

        btnAddPatient.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mCallBack.switchToIntent("patient");
            }
        });

        btnAdminister.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mCallBack.switchToIntent("administer");
            }
        });

        contentUpdate();

        mRecyclerView.addOnItemTouchListener(new PresciptionsTouchListener(getActivity(), mRecyclerView, new PresciptionsTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Prescription prescription = prescriptionList.get(position);
                mCallBack.swapToPreVerification(prescription.getId());
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return rootView;
    }

    public void contentUpdate(){
        final RealmResults<Prescription> prescriptions = realm.where(Prescription.class).sort("nextAdministrationDue").findAll();
        if(prescriptions != null)
        {
            for (int i = 0; i < prescriptions.size(); i++)
            {
                prescriptionList.add(prescriptions.get(i));
            }
            mAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallBack = (preSwitchListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement preSwitchListener");
        }
    }

}
