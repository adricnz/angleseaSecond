package com.team.scrub.angleseadrugcalculator;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;

/**
 * Created by student on 6/14/2018.
 */

public class Prescription extends RealmObject {
    @PrimaryKey
    private int id;
    private Drug drugPrescribed;
    private float dosage;
    private float standingOrder;
    private float totalMedication;
    private Date datePrescribed;
    private Date nextAdministrationDue;
    private String delivery;
    private int hoursTillNext;
    private Patient patient;

    public float getTotalMedication() {
        return totalMedication;
    }

    public void setTotalMedication(float totalMedication) {
        this.totalMedication = totalMedication;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public int getHoursTillNext() {
        return hoursTillNext;
    }

    public void setHoursTillNext(int hoursTillNext) {
        this.hoursTillNext = hoursTillNext;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Drug getDrugPrescribed() {
        return drugPrescribed;
    }

    public void setDrugPrescribed(Drug drugPrescribed) {
        this.drugPrescribed = drugPrescribed;
    }

    public float getDosage() {
        return dosage;
    }

    public void setDosage(float dosage) {
        this.dosage = dosage;
    }

    public float getStandingOrder() {
        return standingOrder;
    }

    public void setStandingOrder(float standingOrder) {
        this.standingOrder = standingOrder;
    }

    public Date getDatePrescribed() {
        return datePrescribed;
    }

    public void setDatePrescribed(Date datePrescribed) {
        this.datePrescribed = datePrescribed;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public Date getNextAdministrationDue() {
        return nextAdministrationDue;
    }

    public void setNextAdministrationDue(Date nextAdministrationDue) {
        this.nextAdministrationDue = nextAdministrationDue;
    }
}
