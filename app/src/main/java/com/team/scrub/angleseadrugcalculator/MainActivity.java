package com.team.scrub.angleseadrugcalculator;

import android.app.FragmentManager;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.ViewParent;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, Administration.adminSwitchListener, PreVerification.preVerificationSwapListener, ViewPatient.preSwitchListener, HomePage.preSwitchListener {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    public Realm realm;
    public RealmConfiguration config;

    //TODO add a delete drug fragment
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);

        Realm.init(this);

        realm = Realm.getDefaultInstance();
        setContentView(R.layout.activity_main);
        final RealmResults<Nurse> count = realm.where(Nurse.class).equalTo("isActive", Boolean.TRUE).findAll();
        if (count.size()==0) {
            Intent logon = new Intent(this, NurseLogon.class);
            startActivity(logon);
        }


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this );
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame,
                        new HomePage())
                .commit();
//        //
//        deleteRealm();
//            addDrugs();
//            addTestPatient(realm);
//
//        updateDrug();
        final RealmResults<Drug> drugs = realm.where(Drug.class).findAll();
        System.out.println(drugs.size()+"drugs results");
        final RealmResults<DrugAdmin> drugAdmins = realm.where(DrugAdmin.class).findAll();
        System.out.println(drugAdmins.size()+"drugAdmins results");


       // realm.beginTransaction();
        final RealmResults<Patient> patients = realm.where(Patient.class).findAll();
        System.out.println("Patients results: "+patients.size());
//
//        Intent i = new Intent(this, NurseNewUser.class);
//        startActivity(i);


    }

    public void updateDrug(){
        realm.beginTransaction();
        Drug d = realm.where(Drug.class).equalTo("name","Droperidol").findFirst();
        d.setAgeGroup("A");
        realm.commitTransaction();
    }


    public void swapToPreVerification(int pId){
        FragmentManager fragmentManager = getFragmentManager();
        PreVerification pv = new PreVerification();

        pv.setPrescriptionId(pId);
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame,
                        pv).commit();
    }

    public void swapToPostVerification(int pId){
        FragmentManager fragmentManager = getFragmentManager();
        PostVerification pv = new PostVerification();

        pv.setPrescriptionId(pId);
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame,
                        pv).commit();
    }

    public void switchToIntent(String intent){
        FragmentManager fragmentManager = getFragmentManager();

        switch(intent){
            case "drug":
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame,
                                new AddDrug())
                        .commit();
                setTitle("Add Drug");
                return;
        case "patient":
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame,
                            new AddPatient())
                    .commit();
            setTitle("Add Patient");
            return;
            case "administer":
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame,
                                new Administration())
                        .commit();
                setTitle("New Administration");
                return;
            default:
                return;
        }


    }


    public void deleteRealm(){
        realm.close();
        Realm.deleteRealm(realm.getConfiguration());
    }

    public void addTestPatient(Realm realm){
        Calendar now = Calendar.getInstance();

        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);
        int day = now.get(Calendar.DAY_OF_MONTH);

        year = 1999;

        now.set(year,month,day);

        String nhi = "GGG333";
        String name = "Edge Case";
        Date dob = now.getTime();
        String room = "Room 14";

        Patient patient = new Patient();
        patient.setDob(dob);
        patient.setName(name);
        patient.setNhiNumber(nhi);
        patient.setRoom(room);

        year = 1900;

        now.set(year,month,day);

        nhi = "GGG222";
        name = "Old Man";
        dob = now.getTime();
        room = "Room 11";

        Patient p2 = new Patient();
        p2.setDob(dob);
        p2.setName(name);
        p2.setNhiNumber(nhi);
        p2.setRoom(room);

        Patient p3 = new Patient();
        year = 2017;

        now.set(year,month,day);

        p3.setDob(now.getTime());
        p3.setName("Babby");
        p3.setNhiNumber("GGG111");
        p3.setRoom("Room 5");

        try {
            realm.beginTransaction();
            final Patient managedPatient = realm.copyToRealm(patient);
            realm.commitTransaction();
            realm.beginTransaction();

            final Patient managedPatient2 = realm.copyToRealm(p2);
            realm.commitTransaction();
            realm.beginTransaction();

            final Patient managedPatient3 = realm.copyToRealm(p3);
            realm.commitTransaction();

        } catch (Exception e){

        }

        final RealmResults<Patient> patients = realm.where(Patient.class).equalTo("name", "John Doe").findAll();
        System.out.println(patients.size()+" patients results");
    }
    public boolean onOptionsItemSelected(MenuItem item){
        if(mToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    public boolean onNavigationItemSelected(@Nullable MenuItem item) {
        FragmentManager fragmentManager = getFragmentManager();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        Intent i;
        switch(item.getItemId())
        {
            case R.id.nurse_logout:
                 i = new Intent(this,NurseLogout.class);
                startActivity(i);
                return true;

            case R.id.add_nurse:
                 i= new Intent(this,NurseLogon.class);
                startActivity(i);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.home:
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame,
                                new HomePage())
                        .commit();
                mDrawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.new_admin:
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame,
                                new Administration())
                        .commit();
                setTitle("New Administration");
                mDrawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.add_patient:
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame,
                                new AddPatient())
                        .commit();
                setTitle("Add Patient");
                mDrawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.add_drug:
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame,
                                new AddDrug())
                        .commit();
                mDrawerLayout.closeDrawer(GravityCompat.START);
                setTitle("Add Drug");
                return true;
            case R.id.edit_drug:
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame,
                                new EditDrug())
                        .commit();
                mDrawerLayout.closeDrawer(GravityCompat.START);
                setTitle("Edit Drug");
                return true;
            case R.id.view_patient:
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame,
                                new ViewPatient())
                        .commit();
                mDrawerLayout.closeDrawer(GravityCompat.START);
                setTitle("View Patient");
                return true;
            case R.id.edit_patient:
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame,
                                new EditPatient())
                        .commit();
                mDrawerLayout.closeDrawer(GravityCompat.START);
                setTitle("Edit Patient");
                return true;
            case R.id.drug_list:
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame,
                                new DrugListFrag())
                        .commit();
                mDrawerLayout.closeDrawer(GravityCompat.START);
                setTitle("Drug List");
            default:
                return false;

        }

    }
    public void drugTransaction(Drug d){
        realm.beginTransaction();
        realm.copyToRealm(d);
        realm.commitTransaction();
    }
    public void addDrugs(){
        Drug d = new Drug();
        d.setId(1);
        d.setName("Paracetamol Syrup 250");
        d.setMl(5);
        d.setMg(250);
        d.setAgeGroup("P");


        Drug d2 = new Drug();
        d2.setId(2);
        d2.setName("Paracetamol Syrup 120");
        d2.setMg(120);
        d2.setMl(5);
        d2.setAgeGroup("P");

        Drug d3 = new Drug();
        d3.setId(3);
        d3.setName("Ibuprofen Syrup");
        d3.setMg(100);
        d3.setMl(5);
        d3.setAgeGroup("P");

        Drug d4 = new Drug();
        d4.setId(4);
        d4.setName("Morphine Syrup/Elixir");
        d4.setMg(100);
        d4.setMl(5);
        d4.setAgeGroup("P");

        Drug d5 = new Drug();
        d5.setId(5);
        d5.setName("Cyclizine");
        d5.setMg(50);
        d5.setMl(1);
        d5.setAgeGroup("A");

        Drug d6 = new Drug();
        d6.setId(6);
        d6.setName("Droperidol");
        d6.setMg(2.5f);
        d6.setMl(1);
        d6.setAgeGroup("A");

        drugTransaction(d);
        drugTransaction(d2);
        drugTransaction(d3);
        drugTransaction(d4);
        drugTransaction(d5);
        drugTransaction(d6);


    }
}
