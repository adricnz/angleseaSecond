package com.team.scrub.angleseadrugcalculator;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmResults;


public class Administration extends android.app.Fragment {


    //my variables
    TextView patientName, patientDOB, roomNumber, patientNhi, patientWeight;
    EditText editDosage, editNhi, editStanding, editHoursRepeat;
    Spinner spnDrugName, spnDeliveryMethod;
    Realm realm;
    Button btnGetDetails, btnSubmit;
    List<String> drugSpinnerArray, deliveryMethodArray;
    adminSwitchListener mCallback;
    Patient currentPatient;
    Float dosage, totalMedication, standing;
    String ageGroup;
    Drug currentDrug;
    View rootView;


    public interface adminSwitchListener {
        public void swapToPreVerification(int pId);
    }


    public Administration() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView =  inflater.inflate(R.layout.fragment_administration, container, false);
        realm = Realm.getDefaultInstance();
        patientName = (TextView)rootView.findViewById(R.id.tv_admin_patient_name);
        patientDOB = (TextView)rootView.findViewById(R.id.tv_admin_patient_dob);
        roomNumber = (TextView)rootView.findViewById(R.id.tv_admin_room_number);
        patientNhi = (TextView)rootView.findViewById(R.id.edit_admin_nhi);
        patientWeight = (TextView)rootView.findViewById(R.id.tv_admin_weight);
        editDosage = (EditText)rootView.findViewById(R.id.edit_admin_dosage);
        spnDeliveryMethod = (Spinner) rootView.findViewById(R.id.spn_admin_delivery);
        btnSubmit = (Button)rootView.findViewById(R.id.btn_admin_submit);
        btnGetDetails = (Button)rootView.findViewById(R.id.btn_admin_get_details);
        editNhi = (EditText)rootView.findViewById(R.id.edit_admin_nhi);
        spnDrugName = (Spinner)rootView.findViewById(R.id.spn_admin_drug);
        editStanding = (EditText)rootView.findViewById(R.id.edit_admin_standing_order);
        editHoursRepeat = (EditText)rootView.findViewById(R.id.edit_admin_repeat);



        deliveryMethodArray = new ArrayList<String>();




        deliveryMethodArray.add("IV");
        deliveryMethodArray.add("Oral");

        ArrayAdapter<String> deliveryMethodAdapter = new ArrayAdapter<String>(
                getActivity(), android.R.layout.simple_spinner_item , deliveryMethodArray);

        spnDeliveryMethod.setAdapter(deliveryMethodAdapter);





        btnGetDetails.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

               getPatientDetails(editNhi.getText().toString());




            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Prescription p = new Prescription();
                //TODO separate to method
                p.setId(getNextKey());

                if(TextUtils.isDigitsOnly(editDosage.getText().toString())){
                    p.setDosage(Float.parseFloat(editDosage.getText().toString()));
                }
                if(TextUtils.isDigitsOnly(editStanding.getText().toString())){
                    p.setStandingOrder(Float.parseFloat(editStanding.getText().toString()));
                }
                p.setDelivery(spnDeliveryMethod.getSelectedItem().toString());
                p.setDatePrescribed(Calendar.getInstance().getTime());
                p.setHoursTillNext(Integer.parseInt(editHoursRepeat.getText().toString()));
                p.setPatient(currentPatient);

                final RealmResults<Drug> drugs = realm.where(Drug.class).equalTo("name",spnDrugName.getSelectedItem().toString()).findAll();


                Drug d = drugs.get(0);
                //TODO error handling
                if(ageGroup.equals("P")){
                    standing = Float.parseFloat(editStanding.getText().toString());
                    dosage = standing * Float.parseFloat(patientWeight.getText().toString());
                    totalMedication = (dosage / d.getMg()) * d.getMl();
                }

                if(ageGroup.equals("A")){
                    dosage = Float.parseFloat(editDosage.getText().toString());
                    totalMedication = (dosage / d.getMg()) * d.getMl();
                }





                Calendar calendar = Calendar.getInstance();

                calendar.add(Calendar.HOUR,p.getHoursTillNext());


                p.setDrugPrescribed(d);
                p.setDosage(dosage);
                p.setTotalMedication(totalMedication);
                p.setNextAdministrationDue(calendar.getTime());

                realm.beginTransaction();
                final Prescription managedPrescription = realm.copyToRealm(p);
                realm.commitTransaction();

                mCallback.swapToPreVerification(managedPrescription.getId());

            }
        });
        return rootView;
    }


    public int getNextKey() {
        try {
            Number n = realm.where(Prescription.class).max("id");
            if (n != null) {
                return n.intValue() + 1;
            } else {
                return 0;
            }
        }
        catch(ArrayIndexOutOfBoundsException e){
                return 0;
            }

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (adminSwitchListener) context;
        } catch (ClassCastException e){
            throw new ClassCastException(context.toString()
            + "must implement adminSwitchListener");
        }




    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//+
//    }


    public void getPatientDetails(String inputNhi){
        realm = Realm.getDefaultInstance();
        Patient p = new Patient();


        final RealmResults<Patient> patients = realm.where(Patient.class).equalTo("nhiNumber",inputNhi).findAll();
        try {
            p = patients.get(0);

        } catch (Exception e){
            Toast.makeText(rootView.getContext(), "No patients found", Toast.LENGTH_SHORT).show();
            return;

        }
        if(patients.size()>1){
            Toast.makeText(rootView.getContext(), "Error. More than one patient found.", Toast.LENGTH_SHORT).show();
            return;
        }
        currentPatient = p;

        // set patient details
        patientName.setText(p.getName());
        roomNumber.setText(p.getRoom());
        patientNhi.setText(p.getNhiNumber());
        patientWeight.setText(Float.toString(p.getWeight()));
        Date dob = p.getDob();

        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        patientDOB.setText(DateFormat.getDateInstance().format(p.getDob()));

        Calendar now = Calendar.getInstance();

        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);
        int day = now.get(Calendar.DAY_OF_MONTH);

        year -= 16;

        now.set(year,month,day);

        if(dob.before(now.getTime())){
            //is adult
            editStanding.setText("Not applicable");
            editStanding.setKeyListener(null);
            ageGroup = "A";


        } else {
            // is paediatric
            editDosage.setText("Will be shown on next screen");
            editDosage.setKeyListener(null);
            ageGroup = "P";
        }

        final RealmResults<Drug> drugs = realm.where(Drug.class).equalTo("ageGroup",ageGroup).findAll();



        drugSpinnerArray = new ArrayList<String>();
        //set drug spinner

        for (Drug drug : drugs){
            drugSpinnerArray.add(drug.getName());
        }
        ArrayAdapter<String> drugAdapter = new ArrayAdapter<String>(
                getActivity(), android.R.layout.simple_spinner_item , drugSpinnerArray);

        spnDrugName.setAdapter(drugAdapter);


    }



}
