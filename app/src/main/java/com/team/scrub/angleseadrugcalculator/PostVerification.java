package com.team.scrub.angleseadrugcalculator;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;


public class PostVerification extends android.app.Fragment {
    View rootView;
    TextView tvNhi, tvName, tvDob, tvRoomNo, tvWeight, tvDrugName, tvStandingOrder, tvDosage, tvTotalMed, tvSeparate,tvDelivery;
    Button btnAdminister;
    Realm realm;
    Patient patient;
    Boolean separateRequired;
    int prescriptionId;
    Prescription p;
    CheckBox checkBox;
    DrugAdmin drugAdmin;

    
    public PostVerification() {
        // Required empty public constructor
    }

    public void setPrescriptionId(int pId){
        prescriptionId = pId;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        realm = Realm.getDefaultInstance();
        
        rootView = inflater.inflate(R.layout.fragment_post_verification, container, false);
        tvNhi = (TextView)rootView.findViewById(R.id.tv_postv_nhi);
        tvName = (TextView)rootView.findViewById(R.id.tv_postv_patient_name);
        tvDob = (TextView)rootView.findViewById(R.id.tv_postv_patient_dob);
        tvRoomNo = (TextView)rootView.findViewById(R.id.tv_postv_room_number);
        tvWeight = (TextView)rootView.findViewById(R.id.tv_postv_weight);
        tvDrugName = (TextView)rootView.findViewById(R.id.tv_postv_drug_name);
        tvStandingOrder = (TextView)rootView.findViewById(R.id.tv_postv_standing_order);
        tvDosage = (TextView)rootView.findViewById(R.id.tv_postv_dosage);
        tvTotalMed = (TextView)rootView.findViewById(R.id.tv_postv_total_med);
        tvSeparate = (TextView)rootView.findViewById(R.id.tv_postv_separate);
        tvDelivery = (TextView)rootView.findViewById(R.id.tv_postv_delivery);
        btnAdminister = (Button)rootView.findViewById(R.id.btn_postv_submit);
        checkBox = (CheckBox)rootView.findViewById(R.id.cb_postv_checkbox);

        setDetails();

        btnAdminister.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                administer();

            }

        });
        return rootView;
    }

    public void administer(){
        if(!separateRequired || checkBox.isChecked()) {
            try {
                final RealmResults<Nurse> nurses = realm.where(Nurse.class).equalTo("isActive", Boolean.TRUE).findAll();
                if (nurses.size() != 2) {
                    Toast.makeText(rootView.getContext(), "Error. Please log a second Nurse in", Toast.LENGTH_SHORT).show();
                    return;
                }
                drugAdmin = new DrugAdmin();
                drugAdmin.setDate(Calendar.getInstance().getTime());
                drugAdmin.setNurse1(nurses.get(0));
                drugAdmin.setNurse2(nurses.get(1));
                drugAdmin.setId(getNextKey());
                drugAdmin.setPrescription(p);

                Calendar calendar = Calendar.getInstance();

                calendar.add(Calendar.HOUR,p.getHoursTillNext());

                realm.beginTransaction();
                p.setNextAdministrationDue(calendar.getTime());
                realm.commitTransaction();

                realm.beginTransaction();
                realm.copyToRealm(drugAdmin);
                realm.commitTransaction();
                Toast.makeText(rootView.getContext(), "Administration successfully lodged", Toast.LENGTH_SHORT).show();




            }catch (Exception e){
                Toast.makeText(rootView.getContext(), "Account Error. Is there a logged in Nurse?", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(rootView.getContext(), "A separate calculation is required. Please check the box", Toast.LENGTH_SHORT).show();

        }
    }
    
    public void setDetails(){
        p = realm.where(Prescription.class).equalTo("id",prescriptionId).findFirst();

        patient = p.getPatient();

        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        tvDob.setText(df.getDateInstance().format(patient.getDob()));
        
        tvNhi.setText(patient.getNhiNumber());
        tvName.setText(patient.getName());
        tvRoomNo.setText(patient.getRoom());
        tvWeight.setText(String.valueOf(patient.getWeight()));
        tvDrugName.setText(p.getDrugPrescribed().getName());
        tvStandingOrder.setText(String.valueOf(p.getStandingOrder()));
        tvDosage.setText(String.valueOf(p.getDosage()));
        tvTotalMed.setText(String.valueOf(p.getTotalMedication()));
        tvDelivery.setText(p.getDelivery());

        if(p.getDrugPrescribed().getAgeGroup().equals("P") || p.getDelivery().equals("IV")
                || p.getDrugPrescribed().getName().equals("Morphine Syrup/Elixir")){
            separateRequired = true;
            tvSeparate.setText("YES");
        } else {
            separateRequired = false;
            tvSeparate.setText("No");
        }




        
    }
    public int getNextKey() {
        try {
            Number n = realm.where(DrugAdmin.class).max("id");
            if (n != null) {
                return n.intValue() + 1;
            } else {
                return 0;
            }
        }
        catch(ArrayIndexOutOfBoundsException e){
            return 0;
        }

    }

}
