package com.team.scrub.angleseadrugcalculator;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class DrugListFrag extends Fragment {

    private RecyclerView mRecyclerView;
    private DrugListAdapter mAdapter;
    private List<Drug> drugList = new ArrayList<>();
    private RadioGroup radioGroup;
    private RadioButton rbPed, rbAdult;

    Realm realm = Realm.getDefaultInstance();
    RealmQuery<Drug> ped = realm.where(Drug.class).equalTo("ageGroup", "P").or().equalTo("ageGroup", "Paediatric");
    RealmQuery<Drug> ad = realm.where(Drug.class).equalTo("ageGroup", "A").or().equalTo("ageGroup", "Adult");

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragement_drug_list, container, false);
        //adapter for the view
        mAdapter = new DrugListAdapter(drugList);
        //getting recycler
        mRecyclerView = (RecyclerView) view.findViewById(R.id.dl_drug_list);
        //setting layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        //giving recycler animation
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //setting a defined size
        mRecyclerView.setHasFixedSize(true);
        //adapter to manage the list
        mRecyclerView.setAdapter(mAdapter);
        //getting radio button group
        radioGroup = (RadioGroup) view.findViewById(R.id.rg_dl);
        //listening for a change in checked
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                //if pediatric is chosen
                if (checkedId == R.id.rb_dl_ped){
                    //clear list
                    drugList.clear();
                    //find all pediatric drugs
                    RealmResults<Drug> pediatric = ped.findAll();
                    //loop to fill list with results
                    if(pediatric != null) {
                        for (int i = 0; i < pediatric.size(); i++) {
                            drugList.add(pediatric.get(i));
                        }
                        //notify a change in dataset
                        mAdapter.notifyDataSetChanged();
                    }
                } else {
                    //clear list
                    drugList.clear();
                    //find all adult drugs
                    RealmResults<Drug> adult = ad.findAll();
                    //loop to fill list with results
                    for (int i = 0; i < adult.size(); i++) {
                        drugList.add(adult.get(i));
                    }
                    //notify a change in dataset
                    mAdapter.notifyDataSetChanged();
                }

            }
        });




        return view;
    }


}
