package com.team.scrub.angleseadrugcalculator;

import android.app.DatePickerDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import io.realm.Realm;
import io.realm.RealmResults;

public class EditPatient extends android.app.Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View rootView;
    EditText editPatientNhi, editPatientName, editDob, editWeight, editRoomNo;
    Button btnGetDetails, btnDob, btnEdit, btnDischarge;
    Realm realm;
    SimpleDateFormat sdf;
    Patient p;


    public void onDateSet(DatePicker view, int year, int month, int day){
        Calendar cal = new GregorianCalendar(year,month,day);
        setDate(cal);
    }
    public void setDate(final Calendar calendar){
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
        ((TextView)rootView.findViewById(R.id.edit_ap_dob)).setText(dateFormat.format(calendar.getTime()));
    }

    public EditPatient() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static EditPatient newInstance(String param1, String param2) {
        EditPatient fragment = new EditPatient();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.fragment_edit_patient, container, false);
        editPatientName = (EditText)rootView.findViewById(R.id.edit_ep_name);
        editPatientNhi = (EditText)rootView.findViewById(R.id.edit_ep_nhi);
        editDob = (EditText)rootView.findViewById(R.id.edit_ep_dob);
        editWeight = (EditText)rootView.findViewById(R.id.edit_ep_weight);
        editRoomNo = (EditText)rootView.findViewById(R.id.edit_ep_room_no);

        btnDob = (Button)rootView.findViewById(R.id.btn_ep_dob);
        btnEdit = (Button)rootView.findViewById(R.id.btn_ep_modify);
        btnGetDetails = (Button)rootView.findViewById(R.id.btn_ep_get_details);
        btnDischarge = (Button)rootView.findViewById(R.id.btn_ep_discharge);

        sdf = new SimpleDateFormat("d-M-y");
        realm = Realm.getDefaultInstance();

        btnDob.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                submit();
            }
        });

        btnGetDetails.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                getPatientDetails(editPatientNhi.getText().toString());
            }
        });

        btnDischarge.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                realm.beginTransaction();
                p.setRoom("Discharged");
            }
        });


        return rootView;
    }

    public void getPatientDetails(String inputNhi) {
        realm = Realm.getDefaultInstance();



        final RealmResults<Patient> patients = realm.where(Patient.class).equalTo("nhiNumber", inputNhi).findAll();
        try {
            p = patients.get(0);
        } catch (Exception e){
            Toast.makeText(rootView.getContext(), "No patients found", Toast.LENGTH_SHORT).show();
            return;
        }
        // set patient details
        editPatientName.setText(p.getName());
        editRoomNo.setText(p.getRoom());
        editWeight.setText(Float.toString(p.getWeight()));
        editPatientNhi.setText(p.getNhiNumber());
        Date dob = p.getDob();

        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        editDob.setText(df.getDateInstance().format(p.getDob()));


    }

    public void submit(){
try {
    realm.beginTransaction();

    p.setWeight(Float.parseFloat(editWeight.getText().toString()));
    p.setName(editPatientName.getText().toString());
    Date date = new Date();
    try {
        date = sdf.parse(editDob.getText().toString());
    } catch (ParseException e) {
        e.printStackTrace();
    }
    p.setDob(date);
    p.setRoom(editRoomNo.getText().toString());

    realm.commitTransaction();
    Toast.makeText(rootView.getContext(), "Patient successfully edited", Toast.LENGTH_SHORT).show();

} catch( Exception e){
    Toast.makeText(rootView.getContext(), "Error. Patient not edited", Toast.LENGTH_SHORT).show();

}
    }

    public void showDatePicker(){
        DatePickerFragment date = new DatePickerFragment();

        Calendar calendar = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calendar.get(Calendar.YEAR));
        args.putInt("month",calendar.get(Calendar.MONTH));
        args.putInt("day",calendar.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);

        date.setCallBack(ondate);
        date.show(getFragmentManager(),"Date Picker");
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }
    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener(){
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth){
            editDob.setText(String.valueOf(dayOfMonth)+"-" + String.valueOf(monthOfYear+1)+"-"+String.valueOf(year));
        }
    };
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
