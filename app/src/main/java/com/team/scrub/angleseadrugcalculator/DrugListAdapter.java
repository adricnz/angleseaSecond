package com.team.scrub.angleseadrugcalculator;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class DrugListAdapter extends RecyclerView.Adapter<DrugListAdapter.ViewHolder> {

    //list of drugs
    private List<Drug> drugList;
    //view holder for each list item
    public static class ViewHolder extends RecyclerView.ViewHolder {
        //stating widgets
        public TextView drugName, dosageAmount;
        //creating single list view
        public ViewHolder(View v){
            super(v);
            drugName = (TextView) v.findViewById(R.id.drug_name);
            dosageAmount = (TextView) v.findViewById(R.id.dosage_amount);
        }
    }
    //constructor
    public DrugListAdapter(List<Drug> drugList) {
        this.drugList = drugList;
    }
    //inflating each item
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.drug_list_row, parent, false);
        return new ViewHolder(itemView);
    }
    //setting the content of each item
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Drug drug = drugList.get(position);
        holder.drugName.setText(drug.getName());
        String ml = String.valueOf(drug.getMg());
        String mg = String.valueOf(drug.getMg());
        String complete = ml + "ml \t" + mg + "mg";
        holder.dosageAmount.setText(complete);
    }
    //item count for the size of list
    @Override
    public int getItemCount() {
        return drugList.size();
    }
}
