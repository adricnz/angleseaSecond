package com.team.scrub.angleseadrugcalculator;

import android.app.DatePickerDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import io.realm.Realm;


public class AddPatient extends android.app.Fragment implements DatePickerDialog.OnDateSetListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View rootView;

    //my Variables
    EditText editPatientName, editDob, editNhi, editWeight, editRoomNo;
    Button btnAdd, btnDob;
    Realm realm;
    SimpleDateFormat sdf;


    public void onDateSet(DatePicker view, int year, int month, int day){
        Calendar cal = new GregorianCalendar(year,month,day);
        setDate(cal);
    }
    public void setDate(final Calendar calendar){
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
        ((TextView)rootView.findViewById(R.id.edit_ap_dob)).setText(dateFormat.format(calendar.getTime()));
    }

    public AddPatient() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddPatient.
     */
    // TODO: Rename and change types and number of parameters
    public static AddPatient newInstance(String param1, String param2) {
        AddPatient fragment = new AddPatient();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_add_patient, container, false);

        editDob = (EditText)rootView.findViewById(R.id.edit_ap_dob);
        editNhi = (EditText)rootView.findViewById(R.id.edit_ap_nhi);
        editPatientName = (EditText)rootView.findViewById(R.id.edit_ap_name);
        editRoomNo = (EditText)rootView.findViewById(R.id.edit_ap_room_no);
        editWeight = (EditText)rootView.findViewById(R.id.edit_ap_weight);
        btnAdd = (Button)rootView.findViewById(R.id.btn_ap_add);
        btnDob = (Button)rootView.findViewById(R.id.btn_ap_dob);
        sdf = new SimpleDateFormat("d-M-y");
        realm = Realm.getDefaultInstance();


        btnAdd.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        submit();
                    }
                }
        );

        btnDob.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        showDatePicker();
                    }
                }
        );



        return rootView;
    }

    public void submit(){

        try {
            realm.beginTransaction();
            Patient p = new Patient();
            p.setWeight(Float.parseFloat(editWeight.getText().toString()));
            p.setNhiNumber(editNhi.getText().toString());
            p.setName(editPatientName.getText().toString());
            Date date = new Date();
            try {
                date = sdf.parse(editDob.getText().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            p.setDob(date);
            p.setRoom(editRoomNo.getText().toString());
            realm.copyToRealm(p);


            realm.commitTransaction();
            Toast.makeText(rootView.getContext(), "Patient successfully created", Toast.LENGTH_SHORT).show();

        } catch (Exception e){
            Toast.makeText(rootView.getContext(), "Error creating patient", Toast.LENGTH_SHORT).show();

        }

    }

    public void showDatePicker(){
        DatePickerFragment date = new DatePickerFragment();

        Calendar calendar = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calendar.get(Calendar.YEAR));
        args.putInt("month",calendar.get(Calendar.MONTH));
        args.putInt("day",calendar.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);

        date.setCallBack(ondate);
        date.show(getFragmentManager(),"Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener(){
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth){
            editDob.setText(String.valueOf(dayOfMonth)+"-" + String.valueOf(monthOfYear+1)+"-"+String.valueOf(year));
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

}
