package com.team.scrub.angleseadrugcalculator;

import io.realm.RealmObject;

/**
 * Created by student on 6/14/2018.
 */

public class Nurse extends RealmObject {
    private String rnNumber;
    private byte[] nurseSig;
    private String name;
    private String password;
    private boolean isActive;

    public Nurse() {
    }

    public String getRnNumber() {
        return rnNumber;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        this.isActive = active;
    }

    public void setRnNumber(String rnNumber) {
        this.rnNumber = rnNumber;
    }

    public byte[] getNurseSig() {
        return nurseSig;
    }

    public void setNurseSig(byte[] nurseSig) {
        this.nurseSig = nurseSig;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
