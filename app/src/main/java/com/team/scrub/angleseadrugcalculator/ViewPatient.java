package com.team.scrub.angleseadrugcalculator;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;


public class ViewPatient extends android.app.Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private EditText editNhi;
    private TextView patientName, patientDob, roomNumber, weight;
    private Button btnGetDetails;
    View rootView;
    Realm realm;
    private RecyclerView mRecyclerView;
    private PrescriptionListAdapter mAdapter;
    private List<Prescription> prescriptionList = new ArrayList<>();

    preSwitchListener mCallBack;

    public interface preSwitchListener {
       public void swapToPreVerification(int pId);
    }

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public ViewPatient() {
        // Required empty public constructor
    }

    public static ViewPatient newInstance(String param1, String param2) {
        ViewPatient fragment = new ViewPatient();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_view_patient, container, false);
        btnGetDetails = (Button)rootView.findViewById(R.id.btn_vp_get_patient);
        patientName = (TextView)rootView.findViewById(R.id.tv_vp_name);
        patientDob = (TextView)rootView.findViewById(R.id.tv_vp_dob);
        roomNumber = (TextView)rootView.findViewById(R.id.tv_vp_room_no);
        weight = (TextView)rootView.findViewById(R.id.tv_vp_weight);
        editNhi = (EditText)rootView.findViewById(R.id.edit_vp_nhi);
        realm = Realm.getDefaultInstance();

        //recycler code
        mAdapter = new PrescriptionListAdapter(prescriptionList);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.pl_prescription_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);

        btnGetDetails.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                getPatientDetails(editNhi.getText().toString());
            }
        });

        mRecyclerView.addOnItemTouchListener(new PresciptionsTouchListener(getActivity(), mRecyclerView, new PresciptionsTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Prescription prescription = prescriptionList.get(position);
                mCallBack.swapToPreVerification(prescription.getId());
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        return rootView;

    }

    public void getPatientDetails(String inputNhi) {
        realm = Realm.getDefaultInstance();
        Patient p;



        final RealmResults<Patient> patients = realm.where(Patient.class).equalTo("nhiNumber", inputNhi).findAll();
        try {
            p = patients.get(0);
        } catch (Exception e){
            Toast.makeText(rootView.getContext(), "No patients found", Toast.LENGTH_SHORT).show();
            return;
        }

        // set patient details
        patientName.setText(p.getName());
        roomNumber.setText(p.getRoom());
        weight.setText(Float.toString(p.getWeight()));
        Date dob = p.getDob();

        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        patientDob.setText(df.getDateInstance().format(p.getDob()));

        final RealmResults<Prescription> prescriptions = realm.where(Prescription.class).equalTo("patient.nhiNumber", p.getNhiNumber()).findAll();
        if(prescriptions != null)
        {
            for (int i = 0; i < prescriptions.size(); i++)
            {
                prescriptionList.add(prescriptions.get(i));
            }
            mAdapter.notifyDataSetChanged();
        }



    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallBack = (preSwitchListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
            + " must implement preSwitchListener");
        }
    }

//    @Override
//    public void onDetach() {
//        super.onDetach();
//    }


}
