package com.team.scrub.angleseadrugcalculator;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


/**
 * Created by student on 6/14/2018.
 */

public class Patient extends RealmObject {

    @PrimaryKey
    private String nhiNumber;
    private String name;
    private float weight;
    private Date dob;
    private String room;
    private RealmList<Prescription> prescriptions;


    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public String getNhiNumber() {
        return nhiNumber;
    }

    public String getName() {
        return name;
    }

    public Date getDob() {
        return dob;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public void setNhiNumber(String nhiNumber) {
        this.nhiNumber = nhiNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }
}

