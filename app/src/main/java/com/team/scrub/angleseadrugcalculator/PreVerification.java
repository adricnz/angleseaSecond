package com.team.scrub.angleseadrugcalculator;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.FloatMath;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;

public class PreVerification extends android.app.Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    //my variables
    TextView patientName, patientDOB, roomNumber, patientNhi, patientWeight, drugName, standingOrder, dosage,totalMedication,deliveryMethod, separateCalculation;
    Button btnSubmit;
    CheckBox checkBox;
    int prescriptionId;
    Realm realm;
    double totalMedicationFloat, roundedTotal, remainder;
    Boolean separateRequired;
    preVerificationSwapListener mCallback;
    View rootView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    public PreVerification() {
        // Required empty public constructor
    }

    public interface preVerificationSwapListener{
        public void swapToPostVerification(int pId);
    }

    public static PreVerification newInstance(String param1, String param2) {
        PreVerification fragment = new PreVerification();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView =  inflater.inflate(R.layout.fragment_pre_verification, container, false);

        // Inflate the layout for this fragment
        patientName = (TextView)rootView.findViewById(R.id.tv_pv_patient_name);
        patientDOB = (TextView)rootView.findViewById(R.id.tv_pv_patient_dob);
        roomNumber = (TextView)rootView.findViewById(R.id.tv_pv_room_number);
        patientNhi = (TextView)rootView.findViewById(R.id.tv_pv_nhi);
        patientWeight = (TextView)rootView.findViewById(R.id.tv_pv_weight);
        drugName = (TextView)rootView.findViewById(R.id.tv_pv_drug_name);
        standingOrder = (TextView)rootView.findViewById(R.id.tv_pv_standing_order);
        dosage = (TextView)rootView.findViewById(R.id.tv_pv_dosage);
        totalMedication = (TextView)rootView.findViewById(R.id.tv_pv_total_med);
        deliveryMethod = (TextView)rootView.findViewById(R.id.tv_pv_delivery);
        separateCalculation = (TextView)rootView.findViewById(R.id.tv_pv_separate);
        checkBox = (CheckBox)rootView.findViewById(R.id.cb_pv_checkbox);

        btnSubmit = (Button)rootView.findViewById(R.id.btn_pv_submit);
        checkBox = (CheckBox)rootView.findViewById(R.id.cb_pv_checkbox);

        btnSubmit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                submit();

            }
        });
        setDetails(prescriptionId);
        System.out.println(mParam1);
        return rootView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mCallback = (PreVerification.preVerificationSwapListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + "must implement preVerificationSwapListener");
        }


        }
    public void setPrescriptionId(int pId){
        prescriptionId = pId;
    }
    public void setDetails(int pId){
        realm = Realm.getDefaultInstance();
        Prescription mPrescription;
        Patient mPatient;

        final RealmResults<Prescription> prescriptions = realm.where(Prescription.class).equalTo("id",pId).findAll();
        mPrescription = prescriptions.get(0);
        mPatient = mPrescription.getPatient();
        if(mPatient!=null) {
                patientName.setText(mPatient.getName());
                patientWeight.setText(Float.toString(mPatient.getWeight()));
                patientDOB.setText(mPatient.getDob().toString());
                roomNumber.setText(mPatient.getRoom());
                patientNhi.setText(mPatient.getNhiNumber());
                drugName.setText(mPrescription.getDrugPrescribed().getName().toString());
                standingOrder.setText(Float.toString(mPrescription.getStandingOrder()));
                dosage.setText(Float.toString(mPrescription.getDosage()));
                deliveryMethod.setText(mPrescription.getDelivery());

            Date dob = mPatient.getDob();

            DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
            patientDOB.setText(df.getDateInstance().format(mPatient.getDob()));

                totalMedicationFloat = mPrescription.getTotalMedication();

                if(mPrescription.getDrugPrescribed().getAgeGroup().equals("P") || mPrescription.getDelivery().equals("IV")
                        || mPrescription.getDrugPrescribed().getName().equals("Morphine Syrup/Elixir")){
                    separateRequired = true;
                    separateCalculation.setText("YES");
                } else {
                    separateRequired = false;
                    separateCalculation.setText("No");
                }

            if(totalMedicationFloat < 0.5){
                roundedTotal = 1;
            } else {
                roundedTotal = Math.round(totalMedicationFloat);
            }

            totalMedication.setText(String.valueOf(mPrescription.getTotalMedication()+" / "+String.valueOf(roundedTotal)));
        }

        //TODO error handling


    }

    public void submit(){
        if(!separateRequired || checkBox.isChecked()) {
            mCallback.swapToPostVerification(prescriptionId);
        } else {
            Toast.makeText(rootView.getContext(), "A separate calculation is required. Please check the box", Toast.LENGTH_SHORT).show();

        }

    }



    @Override
    public void onDetach() {
        super.onDetach();

    }

}
