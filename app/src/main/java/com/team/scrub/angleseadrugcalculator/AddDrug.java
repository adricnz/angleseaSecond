package com.team.scrub.angleseadrugcalculator;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public class AddDrug extends android.app.Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    //my variables
    EditText drugName, drugMg, drugMl;
    Button btnSubmit;
    Realm realm;
    Spinner spnAgeGroup;
    List<String> ageGroupArray;
    View rootView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AddDrug() {
        // Required empty public constructor
    }
    
    public static AddDrug newInstance(String param1, String param2) {
        AddDrug fragment = new AddDrug();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.fragment_add_drug, container, false);

        drugName = (EditText)rootView.findViewById(R.id.edit_ad_drug_name);
        drugMg = (EditText)rootView.findViewById(R.id.edit_ad_drug_mg);
        drugMl = (EditText)rootView.findViewById(R.id.edit_ad_drug_ml);
        btnSubmit = (Button)rootView.findViewById(R.id.btn_ad_submit);
        ageGroupArray = new ArrayList<String>();

        realm = Realm.getDefaultInstance();

        btnSubmit.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        submit();
                    }
                }
        );
        spnAgeGroup = (Spinner)rootView.findViewById(R.id.spn_ad_age_group);

        ageGroupArray.add("Paediatric");
        ageGroupArray.add("Adult");

        ArrayAdapter<String> ageGroupAdapter = new ArrayAdapter<String>(
                getActivity(),android.R.layout.simple_spinner_item, ageGroupArray);

        spnAgeGroup.setAdapter(ageGroupAdapter);
        return rootView;
    }

    public void submit(){
        try {
            realm.beginTransaction();


            Drug d = new Drug();
            d.setName(drugName.getText().toString());
            d.setId(getNextKey());
            d.setMg(Float.parseFloat(drugMg.getText().toString()));
            d.setMl(Float.parseFloat(drugMl.getText().toString()));
            d.setAgeGroup(spnAgeGroup.getSelectedItem().toString());
            realm.copyToRealm(d);
            realm.commitTransaction();
            Toast.makeText(rootView.getContext(), "Drug successfully created", Toast.LENGTH_SHORT).show();

        } catch (Exception e){
            Toast.makeText(rootView.getContext(), "Error. Drug not created", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    public int getNextKey() {
        try {
            Number n = realm.where(Drug.class).max("id");
            if (n != null) {
                return n.intValue() + 1;
            } else {
                return 0;
            }
        }
        catch(ArrayIndexOutOfBoundsException e){
            return 0;
        }

    }



    @Override
    public void onDetach() {
        super.onDetach();
    }


}
