package com.team.scrub.angleseadrugcalculator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import io.realm.Realm;

import io.realm.RealmResults;

import static java.lang.Boolean.FALSE;

public class NurseLogout extends AppCompatActivity{
    TextView tvName1, tvName2, tvRNNumber1, tvRNNumber2;
    Button btnNurse1, btnNurse2, btnBoth;

    String Name1, Name2, RNNumber1, RNNumber2;
    Realm realm;
    Nurse n1, n2;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nurse_logout);
        Realm.init(this);
        setTitle("Nurse Logout");

        tvName1 = (TextView) findViewById(R.id.tv_Nurse1);
        tvName2 = (TextView) findViewById(R.id.tv_Nurse2);
        tvRNNumber1 = (TextView) findViewById(R.id.tv_Nurse1_RNNumber);
        tvRNNumber2 = (TextView) findViewById(R.id.tv_Nurse2_RNNumber);
        btnNurse1 = (Button) findViewById(R.id.btn_Logout1);
        btnNurse2 = (Button) findViewById(R.id.btn_Nurse2);
        btnBoth = (Button) findViewById(R.id.btn_logout_both);

        Realm.init(this);
        realm = Realm.getDefaultInstance();
        final RealmResults<Nurse> nurse = realm.where(Nurse.class).equalTo("isActive", Boolean.TRUE).findAll();

        n1 = nurse.get(0);
        Name1 = n1.getName();
        RNNumber1 = n1.getRnNumber();
        tvName1.setText(Name1);
        tvRNNumber1.setText(RNNumber1);
        btnNurse1.setText("Logout " + Name1);
        //Checks to see if second Nurse is active then makes the option to sign out of the second user only visible.
        if(nurse.size() > 1) {
            n2 = nurse.get(1);
            RNNumber2 = n2.getRnNumber();
            Name2 = n2.getName();
            tvRNNumber2.setVisibility(View.VISIBLE);
            tvName2.setVisibility(View.VISIBLE);
            btnNurse2.setVisibility(View.VISIBLE);
            tvName2.setText(Name2);
            tvRNNumber2.setText(RNNumber2);
            btnNurse2.setText("Logout " + Name2);

        }
    //Logout 1 nurse
        btnNurse1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                realm.beginTransaction();
                n1.setActive(FALSE);
                realm.copyToRealm(n1);
                realm.commitTransaction();
                Intent first = new Intent(NurseLogout.this, NurseLogon.class);
                startActivity(first);

            }
        });
        //Logout 2 nurse
        btnNurse2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                realm.beginTransaction();
                n2.setActive(FALSE);
                realm.copyToRealm(n2);
                realm.commitTransaction();
                Intent first = new Intent(NurseLogout.this, NurseLogon.class);
                startActivity(first);
            }
        });
        //Logout Both nurse
        btnNurse2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                realm.beginTransaction();
                n2.setActive(FALSE);
                realm.copyToRealm(n2);
                realm.commitTransaction();
                realm.beginTransaction();
                n1.setActive(FALSE);

                realm.copyToRealm(n1);
                realm.commitTransaction();
                Intent first = new Intent(NurseLogout.this, NurseLogon.class);
                startActivity(first);
            }
        });



    }
}
