package com.team.scrub.angleseadrugcalculator;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;


public class EditDrug extends android.app.Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View rootView;
    List<String> drugSpinnerArray, deliveryMethodArray, ageGroupArray;
    Spinner spnDrugName, spnAgeGroup;
    Button btnGetDetails, btnSubmit;
    EditText editDrugName, editDrugMg, editDrugMl;
    Realm realm;
    Drug d;



    public EditDrug() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EditDrug.
     */
    // TODO: Rename and change types and number of parameters
    public static EditDrug newInstance(String param1, String param2) {
        EditDrug fragment = new EditDrug();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_edit_drug, container, false);
        btnGetDetails = (Button)rootView.findViewById(R.id.btn_ed_get_details);
        btnSubmit = (Button)rootView.findViewById(R.id.btn_ed_submit);
        editDrugMg = (EditText)rootView.findViewById(R.id.edit_ed_drug_mg);
        editDrugMl = (EditText)rootView.findViewById(R.id.edit_ed_drug_ml);
        editDrugName = (EditText)rootView.findViewById(R.id.edit_ed_drug_name);
        spnDrugName = (Spinner) rootView.findViewById(R.id.spn_ed_drug);
        spnAgeGroup = (Spinner)rootView.findViewById(R.id.spn_ed_age_group);
        realm = Realm.getDefaultInstance();

        btnGetDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDetails();
            }

        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });

        drugSpinnerArray = new ArrayList<String>();
        deliveryMethodArray = new ArrayList<String>();
        ageGroupArray = new ArrayList<String>();
        //set drug spinner
        final RealmResults<Drug> drugs = realm.where(Drug.class).findAll();
        for (Drug drug : drugs){
            drugSpinnerArray.add(drug.getName());
        }

        ArrayAdapter<String> drugAdapter = new ArrayAdapter<String>(
                getActivity(), android.R.layout.simple_spinner_item , drugSpinnerArray);

        spnDrugName.setAdapter(drugAdapter);



        ageGroupArray.add("Paediatric");
        ageGroupArray.add("Adult");

        ArrayAdapter<String>ageGroupAdapter = new ArrayAdapter<String>(
            getActivity(),android.R.layout.simple_spinner_item, ageGroupArray);

        spnAgeGroup.setAdapter(ageGroupAdapter);

        return rootView;
    }

    private void getDetails(){

        final RealmResults<Drug> drugs = realm.where(Drug.class).equalTo("name",spnDrugName.getSelectedItem().toString()).findAll();
        if(drugs.size()>1){
            System.out.println("More than one drug with same name");
            return;
        }
        d = drugs.get(0);
        editDrugMg.setText(Float.toString(d.getMg()));
        editDrugMl.setText(String.valueOf(d.getMl()));
        editDrugName.setText(String.valueOf(d.getName()));

        if(d.getAgeGroup().equals("P")){
            spnAgeGroup.setSelection(0);
        } else if(d.getAgeGroup().equals("A")){
            spnAgeGroup.setSelection(1);
        }



    }

    private void submit(){
        try {
            realm.beginTransaction();
            if (spnAgeGroup.getSelectedItem().toString().equals("Adult")) {
                d.setAgeGroup("A");
            } else {
                d.setAgeGroup("P");
            }
            d.setMg(Float.parseFloat(editDrugMg.getText().toString()));
            d.setMl(Float.parseFloat(editDrugMl.getText().toString()));
            d.setName(editDrugName.getText().toString());
            realm.commitTransaction();
            Toast.makeText(rootView.getContext(), "Drug successfully edited", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            Toast.makeText(rootView.getContext(), "Error. Drug not edited", Toast.LENGTH_SHORT).show();

        }
        drugSpinnerArray = new ArrayList<String>();
        final RealmResults<Drug> drugs = realm.where(Drug.class).findAll();
        for (Drug drug : drugs) {
            drugSpinnerArray.add(drug.getName());
        }

        ArrayAdapter<String> drugAdapter = new ArrayAdapter<String>(
                getActivity(), android.R.layout.simple_spinner_item , drugSpinnerArray);
        spnDrugName.setAdapter(drugAdapter);


        }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

}
