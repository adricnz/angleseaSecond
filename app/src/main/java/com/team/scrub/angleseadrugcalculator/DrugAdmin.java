package com.team.scrub.angleseadrugcalculator;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by student on 6/14/2018.
 */

public class DrugAdmin extends RealmObject {
    @PrimaryKey
    private int id;
    private Prescription prescription;
    private Date date;
    private Nurse nurse1;
    private Nurse nurse2;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Prescription getPrescription() {
        return prescription;
    }

    public void setPrescription(Prescription prescription) {
        this.prescription = prescription;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Nurse getNurse1() {
        return nurse1;
    }

    public void setNurse1(Nurse nurse1) {
        this.nurse1 = nurse1;
    }

    public Nurse getNurse2() {
        return nurse2;
    }

    public void setNurse2(Nurse nurse2) {
        this.nurse2 = nurse2;
    }
}
