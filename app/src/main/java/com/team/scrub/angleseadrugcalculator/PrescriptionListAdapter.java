package com.team.scrub.angleseadrugcalculator;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class PrescriptionListAdapter extends RecyclerView.Adapter<PrescriptionListAdapter.ViewHolder> {

    private List<Prescription> prescriptionList;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView drugName, date, nextAdmin;

        public ViewHolder(View v) {
            super(v);
            drugName = (TextView) v.findViewById(R.id.drug_name);
            date = (TextView) v.findViewById(R.id.date);
            nextAdmin = (TextView) v.findViewById(R.id.next_admin);
        }
    }

    public PrescriptionListAdapter(List<Prescription> prescriptionList) {
        this.prescriptionList = prescriptionList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.prescription_list_row, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Prescription prescription = prescriptionList.get(position);
        holder.drugName.setText(prescription.getDrugPrescribed().getName());
        String dateTemp = DateFormat.getDateInstance().format(prescription.getDatePrescribed());
        DateFormat df = new SimpleDateFormat("MMM d, hh:mm");
        holder.date.setText("Prescribed: " + dateTemp);
        String dTemp = df.format(prescription.getNextAdministrationDue());
        holder.nextAdmin.setText(dTemp);
    }

        @Override
        public int getItemCount() {
            return prescriptionList.size();
        }
    }

